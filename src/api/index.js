/* eslint-disable */
import Vue from "vue";
import * as basicMaterial from './basicdata/basicMaterial'
import * as categorys from './basicdata/categorys'
import * as incomeCategory from './basicdata/incomeCategory'
import * as paymentMethod from './basicdata/paymentMethod'
import * as productCategory from './basicdata/productCategory'
import * as productionType from './basicdata/productionType'
import * as spendingCategory from './basicdata/spendingCategory'
import * as unit from './basicdata/unit'
import * as dept from './basicdata/dept'
import * as product from './basicdata/product'
import * as brand from './basicdata/brand'
import * as companyAccount from './basicdata/companyAccount'
import * as supplier from './supplier/supplier'
import * as supplierCategory from './supplier/supplierCategory'
import * as supplierProductRange from './supplier/supplierProductRange'
import * as customerCategory from './customer/customerCategory'
import * as customer from './customer/customer'
import * as customerProductRange from './customer/customerProductRange'
import * as warehouse from './warehouse/warehouse'
import * as user from './system/user'
import * as role from './system/role'
import * as resource from './system/resource'
import * as accPeriod from './system/accPeriod'
import * as session from './system/session'
import * as systemLog from './system/systemLog'
import * as selfCompany from './system/selfCompany'
import * as po from './po/po'
import * as pw from './po/pw'
import * as poDetail from './po/poDetail'
import * as common from './commonApi/common'
import * as sc from './sales/sc'
import * as so from './sales/so'
import { sort } from "semver";

Vue.prototype.$api = {
  ...basicMaterial,
  ...categorys,
  ...incomeCategory,
  ...paymentMethod,
  ...productCategory,
  ...productionType,
  ...spendingCategory,
  ...unit,
  ...dept,
  ...supplier,
  ...supplierCategory,
  ...supplierProductRange,
  ...customerCategory,
  ...customer,
  ...customerProductRange,
  ...product,
  ...brand,
  ...companyAccount,
  ...warehouse,
  ...user,
  ...role,
  ...resource,
  ...accPeriod,
  ...session,
  ...systemLog,
  ...selfCompany,
  ...po,
  ...pw,
  ...poDetail,
  ...common,
  ...sc,
  ...pw,
  ...so
}