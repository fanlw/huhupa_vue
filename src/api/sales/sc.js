import fetch from '../commonApi/axiosObj'

export function fetchAllScBill () {
  return fetch({
    url: '/scBill/getAll',
    method: 'get'
  })
}

export function getScBillPage (params) {
  return fetch({
    url: '/scBill/getPage',
    method: 'post',
    data: params
  })
}

export function addScBill (data) {
  return fetch({
    url: '/scBill/add',
    method: 'post',
    data
  })
}

export function editScBill (data) {
  return fetch({
    url: '/scBill/edit',
    method: 'put',
    data
  })
}

export function deleteScBill (cid) {
  return fetch({
    url: `/scBill/delete/${cid}`,
    method: 'delete'
  })
}

export function updateBillStatusScBill (data) {
  return fetch({
    url: '/scBill/updateBillStatus',
    method: 'put',
    data
  })
}
