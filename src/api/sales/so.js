import fetch from '../commonApi/axiosObj'

export function fetchAllSoBill () {
  return fetch({
    url: '/soBill/getAll',
    method: 'get'
  })
}

export function getSoBillPage (params) {
  return fetch({
    url: '/soBill/getPage',
    method: 'post',
    data: params
  })
}

export function addSoBill (data) {
  return fetch({
    url: '/soBill/add',
    method: 'post',
    data
  })
}

export function editSoBill (data) {
  return fetch({
    url: '/soBill/edit',
    method: 'put',
    data
  })
}

export function deleteSoBill (cid) {
  return fetch({
    url: `/soBill/delete/${cid}`,
    method: 'delete'
  })
}

export function updateBillStatusSoBill (data) {
  return fetch({
    url: '/soBill/updateBillStatus',
    method: 'put',
    data
  })
}
