import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchAllUnit () {
  return fetch({
    url: '/unit/getAll',
    method: 'get'
  })
}

export function getUnitPage (params) {
  return fetch({
    url: '/unit/getPage',
    method: 'post',
    data: params
  })
}

export function addUnit (data) {
  return fetch({
    url: '/unit/add',
    method: 'post',
    data
  })
}

export function editUnit (data) {
  return fetch({
    url: '/unit/edit',
    method: 'put',
    data
  })
}

export function deleteUnit (cid) {
  return fetch({
    url: `/unit/delete/${cid}`,
    method: 'delete'
  })
}
