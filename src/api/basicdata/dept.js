import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchAllDept () {
  return fetch({
    url: '/dept/getAll',
    method: 'get'
  })
}

export function getDeptPage (params) {
  return fetch({
    url: '/dept/getPage',
    method: 'post',
    data: params
  })
}

export function addDept (data) {
  return fetch({
    url: '/dept/add',
    method: 'post',
    data
  })
}

export function editDept (data) {
  return fetch({
    url: '/dept/edit',
    method: 'put',
    data
  })
}

export function deleteDept (cid) {
  return fetch({
    url: `/dept/delete/${cid}`,
    method: 'delete'
  })
}
