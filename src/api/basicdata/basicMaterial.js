import fetch from '../commonApi/axiosObj'

// 接口
export function fetchAllBasicMaterial () {
  return fetch({
    url: '/basicMaterial/getAll',
    method: 'get'
  })
}

export function getBasicMaterialPage (params) {
  return fetch({
    url: '/basicMaterial/getPage',
    method: 'post',
    data: params
  })
}

export function addBasicMaterial (data) {
  return fetch({
    url: '/basicMaterial/add',
    method: 'post',
    data
  })
}

export function editBasicMaterial (data) {
  return fetch({
    url: '/basicMaterial/edit',
    method: 'put',
    data
  })
}

export function deleteBasicMaterial (cid) {
  return fetch({
    url: `/basicMaterial/delete/${cid}`,
    method: 'delete'
  })
}
