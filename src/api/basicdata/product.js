import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchAllProduct () {
  return fetch({
    url: '/product/getAll',
    method: 'get'
  })
}

export function getProductPage (params) {
  return fetch({
    url: '/product/getPage',
    method: 'post',
    data: params
  })
}

export function addProduct (data) {
  return fetch({
    url: '/product/add',
    method: 'post',
    data
  })
}

export function editProduct (data) {
  return fetch({
    url: '/product/edit',
    method: 'put',
    data
  })
}

export function deleteProduct (cid) {
  return fetch({
    url: `/product/delete/${cid}`,
    method: 'delete'
  })
}

export function getProductRange (params) {
  return fetch({
    url: '/product/range',
    method: 'post',
    data: params
  })
}

export function getProductNotRange (params) {
  return fetch({
    url: '/product/notRange',
    method: 'post',
    data: params
  })
}
