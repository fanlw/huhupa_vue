import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchAllCompanyAccount () {
  return fetch({
    url: '/companyAccount/getAll',
    method: 'get'
  })
}

export function getCompanyAccountPage (params) {
  return fetch({
    url: '/companyAccount/getPage',
    method: 'post',
    data: params
  })
}

export function addCompanyAccount (data) {
  return fetch({
    url: '/companyAccount/add',
    method: 'post',
    data
  })
}

export function editCompanyAccount (data) {
  return fetch({
    url: '/companyAccount/edit',
    method: 'put',
    data
  })
}

export function deleteCompanyAccount (cid) {
  return fetch({
    url: `/companyAccount/delete/${cid}`,
    method: 'delete'
  })
}
