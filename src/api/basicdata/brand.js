import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchAllBrand () {
  return fetch({
    url: '/brand/getAll',
    method: 'get'
  })
}

export function getBrandPage (params) {
  return fetch({
    url: '/brand/getPage',
    method: 'post',
    data: params
  })
}

export function addBrand (data) {
  return fetch({
    url: '/brand/add',
    method: 'post',
    data
  })
}

export function editBrand (data) {
  return fetch({
    url: '/brand/edit',
    method: 'put',
    data
  })
}

export function deleteBrand (cid) {
  return fetch({
    url: `/brand/delete/${cid}`,
    method: 'delete'
  })
}
