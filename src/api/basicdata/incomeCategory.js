import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchIncomeCategories () {
  return fetch({
    url: '/incomeCategory/getAll',
    method: 'get'
  })
}

export function getIncomeCategoryPage (params) {
  return fetch({
    url: '/incomeCategory/getPage',
    method: 'post',
    data: params
  })
}

export function addIncomeCategory (data) {
  return fetch({
    url: '/incomeCategory/add',
    method: 'post',
    data
  })
}

export function editIncomeCategory (data) {
  return fetch({
    url: '/incomeCategory/edit',
    method: 'put',
    data
  })
}

export function deleteIncomeCategory (cid) {
  return fetch({
    url: `/incomeCategory/delete/${cid}`,
    method: 'delete'
  })
}
