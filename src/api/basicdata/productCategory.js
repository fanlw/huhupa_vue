import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchProductCategories () {
  return fetch({
    url: '/productCategory/getAll',
    method: 'get'
  })
}

export function getProductCategoryPage (params) {
  return fetch({
    url: '/productCategory/getPage',
    method: 'post',
    data: params
  })
}

export function addProductCategory (data) {
  return fetch({
    url: '/productCategory/add',
    method: 'post',
    data
  })
}

export function editProductCategory (data) {
  return fetch({
    url: '/productCategory/edit',
    method: 'put',
    data
  })
}

export function deleteProductCategory (cid) {
  return fetch({
    url: `/productCategory/delete/${cid}`,
    method: 'delete'
  })
}
