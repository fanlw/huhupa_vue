import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchSpendingCategories () {
  return fetch({
    url: '/spendingCategory/getAll',
    method: 'get'
  })
}

export function getSpendingCategoryPage (params) {
  return fetch({
    url: '/spendingCategory/getPage',
    method: 'post',
    data: params
  })
}

export function addSpendingCategory (data) {
  return fetch({
    url: '/spendingCategory/add',
    method: 'post',
    data
  })
}

export function editSpendingCategory (data) {
  return fetch({
    url: '/spendingCategory/edit',
    method: 'put',
    data
  })
}

export function deleteSpendingCategory (cid) {
  return fetch({
    url: `/spendingCategory/delete/${cid}`,
    method: 'delete'
  })
}
