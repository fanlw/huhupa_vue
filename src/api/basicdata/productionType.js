import fetch from '../commonApi/axiosObj'

// 产品分类接口
export function fetchProductionType () {
  return fetch({
    url: '/productionType/getAll',
    method: 'get'
  })
}

export function getProductionTypePage (params) {
  return fetch({
    url: '/productionType/getPage',
    method: 'post',
    data: params
  })
}

export function addProductionType (data) {
  return fetch({
    url: '/productionType/add',
    method: 'post',
    data
  })
}

export function editProductionType (data) {
  return fetch({
    url: '/productionType/edit',
    method: 'put',
    data
  })
}

export function deleteProductionType (cid) {
  return fetch({
    url: `/productionType/delete/${cid}`,
    method: 'delete'
  })
}
