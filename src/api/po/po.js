import fetch from '../commonApi/axiosObj'

export function fetchAllPoBill () {
  return fetch({
    url: '/poBill/getAll',
    method: 'get'
  })
}

export function getPoBillPage (params) {
  return fetch({
    url: '/poBill/getPage',
    method: 'post',
    data: params
  })
}

export function addPoBill (data) {
  return fetch({
    url: '/poBill/add',
    method: 'post',
    data
  })
}

export function editPoBill (data) {
  return fetch({
    url: '/poBill/edit',
    method: 'put',
    data
  })
}

export function deletePoBill (cid) {
  return fetch({
    url: `/poBill/delete/${cid}`,
    method: 'delete'
  })
}

export function updateBillStatusPoBill (data) {
  return fetch({
    url: '/poBill/updateBillStatus',
    method: 'put',
    data
  })
}
