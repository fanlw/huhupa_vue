import fetch from '../commonApi/axiosObj'

export function getPoBillDetailPage (params) {
  return fetch({
    url: '/poBillDetail/getPage',
    method: 'post',
    data: params
  })
}
