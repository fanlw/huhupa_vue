import fetch from '../commonApi/axiosObj'

export function fetchAllPwBill () {
  return fetch({
    url: '/pwBill/getAll',
    method: 'get'
  })
}

export function getPwBillPage (params) {
  return fetch({
    url: '/pwBill/getPage',
    method: 'post',
    data: params
  })
}

export function addPwBill (data) {
  return fetch({
    url: '/pwBill/add',
    method: 'post',
    data
  })
}

export function editPwBill (data) {
  return fetch({
    url: '/pwBill/edit',
    method: 'put',
    data
  })
}

export function deletePwBill (cid) {
  return fetch({
    url: `/pwBill/delete/${cid}`,
    method: 'delete'
  })
}

export function updateBillStatusPwBill (data) {
  return fetch({
    url: '/pwBill/updateBillStatus',
    method: 'put',
    data
  })
}
