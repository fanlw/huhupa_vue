import fetch from '../commonApi/axiosObj'

export function login (params) {
  return fetch({
    url: '/session/login',
    method: 'post',
    data: params
  })
}

export function logout () {
  return fetch({
    url: `/session/logout`,
    method: 'get'
  })
}

export function getOnlineUsers () {
  return fetch({
    url: `/session/getOnlineUsers`,
    method: 'get'
  })
}

export function getCurrentUser () {
  return fetch({
    url: `/session/getCurrentUser`,
    method: 'get'
  })
}
