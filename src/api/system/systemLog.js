import fetch from '../commonApi/axiosObj'

export function getSystemLogPage (params) {
  return fetch({
    url: '/systemLog/getPage',
    method: 'post',
    data: params
  })
}
