import fetch from '../commonApi/axiosObj'

// 接口
export function fetchAllResource () {
  return fetch({
    url: '/resource/getAll',
    method: 'get'
  })
}

export function getResourcePage (params) {
  return fetch({
    url: '/resource/getPage',
    method: 'post',
    data: params
  })
}

export function getResourceTree (params) {
  return fetch({
    url: '/resource/tree',
    method: 'post',
    data: params
  })
}

export function addResource (data) {
  return fetch({
    url: '/resource/add',
    method: 'post',
    data
  })
}

export function editResource (data) {
  return fetch({
    url: '/resource/edit',
    method: 'put',
    data
  })
}

export function deleteResource (cid) {
  return fetch({
    url: `/resource/delete/${cid}`,
    method: 'delete'
  })
}

export function getResourceRoles (params) {
  return fetch({
    url: `/resource/resourceRoles`,
    method: 'post',
    data: params
  })
}

// export function removeRoles (params) {
//   return fetch({
//     url: `/resource/removeRoles`,
//     method: 'delete',
//     data: params
//   })
// }

export function addRoleToResource (params) {
  return fetch({
    url: `/resource/addRoles`,
    method: 'post',
    data: params
  })
}
