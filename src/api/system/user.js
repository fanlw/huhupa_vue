import fetch from '../commonApi/axiosObj'

// 接口
export function fetchAllUser () {
  return fetch({
    url: '/user/getAll',
    method: 'get'
  })
}

export function getUserPage (params) {
  return fetch({
    url: '/user/getPage',
    method: 'post',
    data: params
  })
}

export function getUserRoles (params) {
  return fetch({
    url: `/user/userRoles`,
    method: 'post',
    data: params
  })
}

export function addRoleToUser (params) {
  return fetch({
    url: `/user/addRoles`,
    method: 'post',
    data: params
  })
}

export function removeUserRoles (params) {
  return fetch({
    url: `/user/removeRoles`,
    method: 'delete',
    data: params
  })
}

export function addUser (data) {
  return fetch({
    url: '/user/add',
    method: 'post',
    data
  })
}

export function editUser (data) {
  return fetch({
    url: '/user/edit',
    method: 'put',
    data
  })
}

export function deleteUser (cid) {
  return fetch({
    url: `/user/delete/${cid}`,
    method: 'delete'
  })
}
