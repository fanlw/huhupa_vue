/**
 * 会计期间 backupRestore
 */

import fetch from '../commonApi/axiosObj'

// 接口
export function fetchAllAccPeriod () {
  return fetch({
    url: '/accPeriod/getAll',
    method: 'get'
  })
}

export function getAccPeriodPage (params) {
  return fetch({
    url: '/accPeriod/getPage',
    method: 'post',
    data: params
  })
}

export function addAccPeriod (data) {
  return fetch({
    url: '/accPeriod/add',
    method: 'post',
    data
  })
}

export function editAccPeriod (data) {
  return fetch({
    url: '/accPeriod/edit',
    method: 'put',
    data
  })
}

export function deleteAccPeriod (cid) {
  return fetch({
    url: `/accPeriod/delete/${cid}`,
    method: 'delete'
  })
}
