import fetch from '../commonApi/axiosObj'

export function fetchAllSelfCompany () {
  return fetch({
    url: '/selfCompany/getAll',
    method: 'get'
  })
}

// 产品分类接口
export function getSelfCompany () {
  return fetch({
    url: '/selfCompany/getSelf',
    method: 'get'
  })
}

export function getSelfCompanyPage (params) {
  return fetch({
    url: '/selfCompany/getPage',
    method: 'post',
    data: params
  })
}

export function addSelfCompany (data) {
  return fetch({
    url: '/selfCompany/add',
    method: 'post',
    data
  })
}

export function editSelfCompany (data) {
  return fetch({
    url: '/selfCompany/edit',
    method: 'put',
    data
  })
}

export function deleteSelfCompany (cid) {
  return fetch({
    url: `/selfCompany/delete/${cid}`,
    method: 'delete'
  })
}
