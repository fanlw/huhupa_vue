import fetch from '../commonApi/axiosObj'

// 接口
export function fetchAllRole () {
  return fetch({
    url: '/role/getAll',
    method: 'get'
  })
}

export function getRolePage (params) {
  return fetch({
    url: '/role/getPage',
    method: 'post',
    data: params
  })
}

export function addRole (data) {
  return fetch({
    url: '/role/add',
    method: 'post',
    data
  })
}

export function editRole (data) {
  return fetch({
    url: '/role/edit',
    method: 'put',
    data
  })
}

export function deleteRole (cid) {
  return fetch({
    url: `/role/delete/${cid}`,
    method: 'delete'
  })
}

export function getRoleUsers (params) {
  return fetch({
    url: `/role/roleUsers`,
    method: 'post',
    data: params
  })
}

export function removeUsers (params) {
  return fetch({
    url: `/role/removeUsers`,
    method: 'delete',
    data: params
  })
}

export function addUserToRole (params) {
  return fetch({
    url: `/role/addUsers`,
    method: 'post',
    data: params
  })
}

export function getRoleResources (roleId) {
  return fetch({
    url: `/role/${roleId}/resources`,
    method: 'get'
  })
}

export function removeResources (params) {
  return fetch({
    url: `/role/removeResources`,
    method: 'delete',
    data: params
  })
}

export function addResourceToRole (params) {
  return fetch({
    url: `/role/addResources`,
    method: 'post',
    data: params
  })
}
