import fetch from '../commonApi/axiosObj'

export function addSupplierCategory (data) {
  return fetch({
    url: '/supplierCategory/add',
    method: 'post',
    data
  })
}

export function getSupplierCategoryById (cid) {
  return fetch({
    url: `/supplierCategory/${cid}`,
    method: 'get'
  })
}

export function fetchAllSupplierCategory () {
  return fetch({
    url: `/supplierCategory/getAll`,
    method: 'get'
  })
}

export function getSupplierCategoryPage (params) {
  return fetch({
    url: '/supplierCategory/getPage',
    method: 'post',
    data: params
  })
}

export function editSupplierCategory (data) {
  return fetch({
    url: '/supplierCategory/edit',
    method: 'put',
    data
  })
}

export function deleteSupplierCategory (pid) {
  return fetch({
    url: `/supplierCategory/delete/${pid}`,
    method: 'delete'
  })
}
