import fetch from '../commonApi/axiosObj'

export function fetchAllSupplierProductRange () {
  return fetch({
    url: '/supplierProductRange/getAll',
    method: 'get'
  })
}

export function getSupplierProductRangePage (params) {
  return fetch({
    url: '/supplierProductRange/getPage',
    method: 'post',
    data: params
  })
}

export function addSupplierProductRange (data) {
  return fetch({
    url: '/supplierProductRange/add',
    method: 'post',
    data
  })
}

export function editSupplierProductRange (data) {
  return fetch({
    url: '/supplierProductRange/edit',
    method: 'put',
    data
  })
}

export function deleteSupplierProductRange (cid) {
  return fetch({
    url: `/supplierProductRange/delete/${cid}`,
    method: 'delete'
  })
}

export function deleteSupplierProductRangeBulk (params) {
  return fetch({
    url: `/supplierProductRange/bulk`,
    method: 'delete',
    data: params
  })
}

export function addSupplierProductRangeBulk (params) {
  return fetch({
    url: `/supplierProductRange/bulk`,
    method: 'post',
    data: params
  })
}
