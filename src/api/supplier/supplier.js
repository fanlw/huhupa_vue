import fetch from '../commonApi/axiosObj'

// 公司管理接口
export function addSupplier (data) {
  return fetch({
    url: '/supplier/add',
    method: 'post',
    data
  })
}

export function getSupplierById (cid) {
  return fetch({
    url: `/supplier/${cid}`,
    method: 'get'
  })
}

export function fetchAllSupplier () {
  return fetch({
    url: `/supplier/getAll`,
    method: 'get'
  })
}

export function editSupplier (data) {
  return fetch({
    url: '/supplier/edit',
    method: 'put',
    data
  })
}

export function deleteSupplier (pid) {
  return fetch({
    url: `/supplier/delete/${pid}`,
    method: 'delete'
  })
}

export function getSupplierPage (params) {
  return fetch({
    url: `/supplier/getPage`,
    method: 'post',
    data: params
  })
}
