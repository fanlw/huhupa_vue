import fetch from './axiosObj'

/**
 * 分页获取指定单据的条目
 */
export function getBillsItemsPage (params) {
  return fetch({
    url: '/bills/items',
    method: 'post',
    data: params
  })
}
