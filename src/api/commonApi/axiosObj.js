import { createAxios } from '@/axios'

const fetch = createAxios(process.env.VUE_APP_BASE_API)

export default fetch
