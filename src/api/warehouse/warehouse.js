import fetch from '../commonApi/axiosObj'

export function fetchAllWarehouse () {
  return fetch({
    url: '/warehouse/getAll',
    method: 'get'
  })
}

export function getWarehousePage (params) {
  return fetch({
    url: '/warehouse/getPage',
    method: 'post',
    data: params
  })
}

export function addWarehouse (data) {
  return fetch({
    url: '/warehouse/add',
    method: 'post',
    data
  })
}

export function editWarehouse (data) {
  return fetch({
    url: '/warehouse/edit',
    method: 'put',
    data
  })
}

export function deleteWarehouse (cid) {
  return fetch({
    url: `/warehouse/delete/${cid}`,
    method: 'delete'
  })
}
