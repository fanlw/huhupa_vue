import fetch from '../commonApi/axiosObj'

export function fetchCustomerCategorys () {
  return fetch({
    url: '/customerCategory/getAll',
    method: 'get'
  })
}

export function getCustomerCategoryPage (params) {
  return fetch({
    url: '/customerCategory/getPage',
    method: 'post',
    data: params
  })
}

export function addCustomerCategorys (data) {
  return fetch({
    url: '/customerCategory/add',
    method: 'post',
    data
  })
}

export function editCustomerCategory (data) {
  return fetch({
    url: '/customerCategory/edit',
    method: 'put',
    data
  })
}

export function deleteCustomerCategory (cid) {
  return fetch({
    url: `/customerCategory/delete/${cid}`,
    method: 'delete'
  })
}
