import fetch from '../commonApi/axiosObj'

export function fetchAllCustomerProductRange () {
  return fetch({
    url: '/customerProductRange/getAll',
    method: 'get'
  })
}

export function getCustomerProductRangePage (params) {
  return fetch({
    url: '/customerProductRange/getPage',
    method: 'post',
    data: params
  })
}

export function addCustomerProductRange (data) {
  return fetch({
    url: '/customerProductRange/add',
    method: 'post',
    data
  })
}

export function editCustomerProductRange (data) {
  return fetch({
    url: '/customerProductRange/edit',
    method: 'put',
    data
  })
}

export function deleteCustomerProductRange (cid) {
  return fetch({
    url: `/customerProductRange/delete/${cid}`,
    method: 'delete'
  })
}

export function deleteCustomerProductRangeBulk (params) {
  return fetch({
    url: `/customerProductRange/bulk`,
    method: 'delete',
    data: params
  })
}

export function addCustomerProductRangeBulk (params) {
  return fetch({
    url: `/customerProductRange/bulk`,
    method: 'post',
    data: params
  })
}
