import fetch from '../commonApi/axiosObj'

export function fetchAllCustomer () {
  return fetch({
    url: '/customer/getAll',
    method: 'get'
  })
}

export function getCustomerPage (params) {
  return fetch({
    url: '/customer/getPage',
    method: 'post',
    data: params
  })
}

export function addCustomer (data) {
  return fetch({
    url: '/customer/add',
    method: 'post',
    data
  })
}

export function editCustomer (data) {
  return fetch({
    url: '/customer/edit',
    method: 'put',
    data
  })
}

export function deleteCustomer (cid) {
  return fetch({
    url: `/customer/delete/${cid}`,
    method: 'delete'
  })
}
